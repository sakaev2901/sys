package repositories;

import config.ConnectionConfig;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.google.gson.Gson;
import models.BlockInfo;

public class DataRepository {
    private static final ConnectionConfig CONNECTION_CONFIG = ConnectionConfig.getInstance();
    private static String SAVE = "INSERT INTO block_info (block_number, data, prev_hash, data_sign, sign, public_key) VALUES (?, ?, ?, ?, ?, ?)";

    public static void save(BlockInfo blockInfo) {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = CONNECTION_CONFIG.getConnection();
            statement = connection.prepareStatement(SAVE);

            statement.setInt(1, blockInfo.getBlockNum());
            statement.setString(2, new Gson().toJson(blockInfo.getData()));
            statement.setBytes(3, blockInfo.getPrevHash());
            statement.setBytes(4, blockInfo.getSignBlock());
            statement.setBytes(5, blockInfo.getSign());
            statement.setString(6, blockInfo.getPublicKey());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        } finally {
            CONNECTION_CONFIG.close(statement);
            CONNECTION_CONFIG.close(connection);
        }
    }
}
