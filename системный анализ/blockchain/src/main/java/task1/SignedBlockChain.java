package task1;

import models.BlockInfo;
import org.bouncycastle.util.encoders.Hex;
import repositories.DataRepository;
import utils.Utils;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class SignedBlockChain {

    private static final int BC_LENGTH = 10;
    private static final List<BlockInfo> blockchain = new ArrayList<>();
    private static KeyPair keyPair;

    static {
        try {
            keyPair = Utils.loadKeys();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    public static void main(String[] args) {

        try {
            makeBlockChain();

            save(blockchain);

            print();

            System.out.println("verification result: " + verification());

            print();

            System.out.println("verification result: " + verification());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void makeBlockChain() {
        byte[] prevHash = null;

        for (int i = 0; i < BC_LENGTH; i++) {
            BlockInfo blockInfo = new BlockInfo(i);
            blockInfo.getData().add("{\"data\":\"data " + i + "\"}");
            blockInfo.getData().add("{\"timestamp\":\"" + new Date() + "\"}");
            blockInfo.setPrevHash(prevHash);

            try {
                byte[] byteData = Utils.stringListToByteArray(blockInfo.getData());
                blockInfo.setSignBlock(Utils.generateRSAPSSSignature(keyPair.getPrivate(), byteData));

                byte[] hash = Utils.getHash(blockInfo);

                blockInfo.setSign(Utils.generateRSAPSSSignature(keyPair.getPrivate(), hash));
                blockInfo.setPublicKey(Utils.byteArrayToHexString(keyPair.getPublic().getEncoded()));
                prevHash = hash;
            } catch (Exception e) {
                e.printStackTrace();
            }

            blockchain.add(blockInfo);
        }
    }

    public static void save(List<BlockInfo> blockInfoList) {
        for (BlockInfo blockInfo : blockInfoList) {
            DataRepository.save(blockInfo);
        }
    }

    private static void print() throws NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException {
        for (int i = 0; i < BC_LENGTH; i++) {
            BlockInfo bi = blockchain.get(i);
            System.out.println("===================== " + bi.getBlockNum() + " =============================");
            System.out.println("prev hash: " + (bi.getPrevHash() != null ? new String(Hex.encode(bi.getPrevHash())) : ""));
            for (String s : bi.getData()) System.out.println(s);
            System.out.println("digest: " + new String(Hex.encode(Utils.getHash(bi))));
            System.out.println("signature: " + new String(Hex.encode(bi.getSign())));
            System.out.println();
        }
    }

    private static boolean verification() throws GeneralSecurityException, UnsupportedEncodingException {

        byte[] prevHash = Utils.getHash(blockchain.get(0));
        for (int i = 1; i < BC_LENGTH; i++) {
            if (!Arrays.equals(prevHash, blockchain.get(i).getPrevHash())) {
                return false;
            }

            prevHash = Utils.getHash(blockchain.get(i));

            if (!Utils.verifyRSAPSSSignature(keyPair.getPublic(), prevHash, blockchain.get(i).getSign())) {
                return false;
            }
        }

        return true;
    }
}
