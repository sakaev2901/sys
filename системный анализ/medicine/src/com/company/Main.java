package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// вывих или ушиб. Осмотр: боль, покраснение, подвижность,

        System.out.println("Введите жалобу: ");
        Scanner scanner = new Scanner(System.in);
        String symp = scanner.nextLine();
        symp = symp.toLowerCase();
        int s1 = 0;
        int s2 = 0;
        if (symp.contains("боль") || symp.contains("болит") || symp.contains("рук") || symp.contains("ног")) {
            s1 += 1;
            s2 += 1;
            if (symp.contains("дикая") || symp.contains("острая") || symp.contains("сильная")) {
                s2 += 1;
            }
        }

        String osmotr = "";
        while (!osmotr.equals("exit")) {
            System.out.println("Введите результаты осмотра: ");
            osmotr = scanner.nextLine();
            if (osmotr.contains("покраснение")) {
                s1 += 1;
                s2 += 1;
            }
            if (osmotr.contains("не может шевелить конечностью")) {
                s1 += 0;
                s2 += 1;
            }
            if (osmotr.contains("может шевелить конечностью")) {
                s1 += 0;
                s2 += 1;
            }
            if (osmotr.contains("торчит кость")) {
                s1 += 0;
                s2 += 100;
                System.out.println("У пациента перелом");
                System.exit(1);
            }
            if (osmotr.contains("покраснение")) {
                s1 += 1;
                s2 += 1;
            }
            if (osmotr.contains("покраснения нет")) {
                s1 += 1;
                s2 += 0;
            }
            if (osmotr.contains("в ренгене целая кость")) {
                s1 += 100;
                s2 += 0;
                System.out.println("У пациента ушиб");
            }


        }

        if (s1 > s2) {
            System.out.println("У пациента ушиб");
        } else {
            System.out.println("У пациента перелом");
        }
    }
}
