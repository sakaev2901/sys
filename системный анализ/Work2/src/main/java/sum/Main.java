package sum;

import org.yaml.snakeyaml.util.ArrayUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    CurrencyData[] eurrub;
    CurrencyData[] jpyrub;
    CurrencyData[] usdrub;
    CurrencyData[] uzsrub;

    // исходная сумма для вложения
    double budget = 1000000;

    // кол-во дней в подвыборке
    final int portion_size = 5;

    final double A = 3000;

    // массив их 5 дневных (portion_size) выборок
    Sample[] sampling;

    // пропорции валют в стратегиях
    double[][] strategy = new double[][]{{0.1,0.15,0.15,0.6}, {0.3,0.3,0.3,0.1},{0.6,0.1,0.1,0.2},{0.35,0.35,0.15,0.15}};

    double[][] strategy2 = new double[][]{{0.2,0.15,0.25,0.4}, {0.3,0.25,0.15,0.3},{0.5,0.15,0.25,0.1},{0.35,0.25,0.20,0.20}};


    // размер всей исследуемой выборки
    int sz;

    public static void main(String[] args) {

        Main prog = new Main();
        // Загружаем данные
        prog.initData();
        // Строим выборку
        prog.sampling();

        prog.makeMatrix();
    }

    public void sampling() {
        // будем рассматривать выборки по portion_size дней из массива котировок
        // затем по ним фиксировать частоту появления событий при разных стратегиях
        sampling = new Sample[sz/portion_size];

        for (int i = 0; i < sz/portion_size; i++) { // цикл по подвыборкам, их количество = размер массива котировок / portion_size

            System.out.println("Выборка #" + i + " дата с: " + new SimpleDateFormat("dd.MM.yyyy").format(eurrub[i * portion_size].date));

            int l = i * portion_size; // индекс первого элемента подвыборки в общем массиве котировок

            // формируем и обсчитываем выборку из 5 (portion_size) дней
            // в цикле индекс очередной подвыборки формируется как [i/portion_size]
            sampling[i] = new Sample();

            // Расчитываем "прибыль"(profit) - разницу между пересчитанной в рублях валютной корзины по
            // курсам закрытия и исходной суммой для каждой стратегии
            // в i-ый день формируем корзину, в (i + portion_size)-ый день конвертируем в рубли
            for (int k = 0; k < 4; k++) { // цикл по стратегиям
                sampling[i].profit[k] = budget * (
                        strategy[k][0]*((1/eurrub[l].open)*eurrub[l + portion_size - 1].close - 1) +
                        strategy[k][1]*((1/jpyrub[l].open)*jpyrub[l + portion_size - 1].close - 1) +
                        strategy[k][2]*((1/usdrub[l].open)*usdrub[l + portion_size - 1].close - 1) +
                        strategy[k][3]*((1/uzsrub[l].open)*uzsrub[l + portion_size - 1].close - 1)
                );
            }

            // Для подсчета дисперсии по пропорциональной совокупности валют в корзине по каждой стратегии
            // сфомируем "курс" корзины на каждый день подвыборки из portion_size дней (для определенности, по цене закрытия)
            // как сумму произведений курсов валют на соответствующую пропорцию из стратегии
            // По такому "курсу" корзины для разных стратегий вычислим среднее и дисперсию
            double max_dispersion = 0; // переменная для сравнения дисперсий разных стратегий
            for (int k = 0; k < 4; k++) { // цикл по стратегиям
                // среднее
                double x_ = 0;
                for (int j = l; j < l + portion_size; j++) {
                    x_ = x_ + strategy[k][0] * eurrub[j].close + strategy[k][1] * jpyrub[j].close +
                            strategy[k][2] * usdrub[j].close + strategy[k][3] * uzsrub[j].close;
                }
                x_ = x_/portion_size;

                // дисперсия
                double d = 0;
                for (int j = l; j < l + portion_size; j++) {
                    d = d + (strategy[k][0] * eurrub[j].close + strategy[k][1] * jpyrub[j].close +
                            strategy[k][2] * usdrub[j].close + strategy[k][3] * uzsrub[j].close - x_) *
                            (strategy[k][0] * eurrub[j].close + strategy[k][1] * jpyrub[j].close +
                                    strategy[k][2] * usdrub[j].close + strategy[k][3] * uzsrub[j].close - x_);
                }
                // зафиксируем дисперсию в структуре, хранящей разные параметры подвыборки
                sampling[i].d[k] = d/portion_size;

                // Проверим, не является ли стратегия наиболее рискованной
                if (sampling[i].d[k] > max_dispersion) {
                    max_dispersion = sampling[i].d[k];
                    sampling[i].risk_strategy = k;
                }
            }

            // Определим состояния "Природы" для стратегий
            for (int k = 0; k < 4; k++) { // цикл по стратегиям
                if (sampling[i].profit[k] > A && k == sampling[i].risk_strategy) {
                    sampling[i].environment_state[k] = 0;
                } else if (sampling[i].profit[k] < A && k == sampling[i].risk_strategy) {
                    sampling[i].environment_state[k] = 1;
                } else if (sampling[i].profit[k] > A && k != sampling[i].risk_strategy) {
                    sampling[i].environment_state[k] = 2;
                } else if (sampling[i].profit[k] < A && k != sampling[i].risk_strategy) {
                    sampling[i].environment_state[k] = 3;
                }
            }

            // распечатаем параметры подвыборки
            for (int k = 0; k < 4; k++) { // цикл по стратегиям
                System.out.println("s" + k + ": profit=" + sampling[i].profit[k] + " , dispersion=" + sampling[i].d[k] + ", environment_state=" + sampling[i].environment_state[k]);
            }
        }
    }

    /*
        Соберем значения в стратегиях с одинаковыми состояниями природы
        например:
        s0 вместе с состоянием  p0 встречается 0 раза
        s0 вместе с состоянием  p1 встречается 0 раза
        s0 вместе с состоянием  p2 встречается 5 раза
        s0 вместе с состоянием  p3 встречается 14 раза
        Тогда средний выигрыш s0 при условии состояния p0 составит 0 руб. с частотой 0
            средний выигрыш s0 при условии состояния p1 составит 0 руб. с частотой 0
            средний выигрыш s0 при условии состояния p2 составит (57419,073814245 / 5)=11483,814762849 руб. с частотой 5/19
            средний выигрыш s0 при условии состояния p3 составит (−60454,60621777 / 14)=−4318,186158412 руб. с частотой 14/19
     */
    public void makeMatrix() {

        System.out.println("----------------------------------------------------------------------------");

        double[][] profits = new double[4][4];
        double[][] probs = new double[4][4];

        for (int k = 0; k < 4; k++) {// перебираем стратегии
            double cp0=0; double cp1=0; double cp2=0; double cp3=0; // счетчики выпавших состояний природы при стратегии k
            double ss0=0; double ss1=0; double ss2=0; double ss3=0; // сумма выигрыша стратегии k при определенном состоянии природы

            for (int i = 0; i < sz / portion_size; i++) { // перебираем выборки и смотрим соответствие стратегии и состояния
                if (sampling[i].environment_state[k] == 0) {
                    cp0++;
                    ss0 += sampling[i].profit[k];
                }
                else if (sampling[i].environment_state[k] == 1) {
                    cp1++;
                    ss1 += sampling[i].profit[k];
                }
                else if (sampling[i].environment_state[k] == 2) {
                    cp2++;
                    ss2 += sampling[i].profit[k];
                }
                else if (sampling[i].environment_state[k] == 3) {
                    cp3++;
                    ss3 += sampling[i].profit[k];
                }
            }

            if (cp0 > 0) {ss0 = ss0/cp0; cp0 = cp0/ (sz / portion_size);}
            if (cp1 > 0) {ss1 = ss1/cp1; cp1 = cp1/ (sz / portion_size);}
            if (cp2 > 0) {ss2 = ss2/cp2; cp2 = cp2/ (sz / portion_size);}
            if (cp3 > 0) {ss3 = ss3/cp3; cp3 = cp3/ (sz / portion_size);}

            profits[k] = new double[]{ss0, ss1, ss2, ss3};
            probs[k] = new double[]{cp0, cp1, cp2, cp3};

            // Выводим строку матрицы
            System.out.println("s"+ k +"|" + ss0 + "; " + cp0 + "|" + ss1 + "; " + cp1 + "|" + ss2 + "; " + cp2 + "|" + ss3 + "; " + cp3 + "|");

            //System.out.println(ss0 * cp0 + ss1 * cp1 + ss2 * cp2 + ss3 * cp3);
        }
        System.out.println("По Баесу: s" + bayes(profits, probs));
        System.out.println("По Вальду: s" + wald(profits));
        System.out.println("По оптимизму: s" + optimism(profits));
        System.out.println("По Гурвицу: s" + hurwitz(profits));
        System.out.println("По Сэвиджу: s" + savage(profits));
    }

    public Integer bayes (double[][] profits, double[][] probs) {
        double maxValue = - Double.MAX_VALUE;
        Integer numStrategy = null;

        for (int i = 0; i < 4; i++){
            double sum = 0;
            for (int j = 0; j < 4; j++) {
                sum += profits[i][j] * probs[i][j];
            }

            if (sum > maxValue) {
                maxValue = sum;
                numStrategy = i;
            }
        }
        return numStrategy;
    }

    public Integer wald(double[][] profits){
        Double[] values = new Double[4];
        for (int i = 0; i < 4; i++){
            values[i] = Arrays.stream(profits[i]).min().getAsDouble();
        }
        List<Double> valueList = new ArrayList<Double>(Arrays.asList(values));
        return (valueList).indexOf(Collections.max(valueList));
    }

    public Integer optimism(double[][] profits) {
        Double[] values = new Double[4];
        for (int i = 0; i < 4; i++){
            values[i] = Arrays.stream(profits[i]).max().getAsDouble();
        }
        List<Double> valueList = new ArrayList<Double>(Arrays.asList(values));
        return (valueList).indexOf(Collections.max(valueList));
    }

    public Integer hurwitz(double[][] profits) {
        Double[] values = new Double[4];
        double opt = 0.4;
        double pes = 1 - opt;
        for (int i =0; i < 4; i++) {
            double min = Arrays.stream(profits[i]).min().getAsDouble();
            double max = Arrays.stream(profits[i]).max().getAsDouble();
            values[i] = pes * min + opt * max;
        }
        List<Double> valueList = new ArrayList<Double>(Arrays.asList(values));
        return (valueList).indexOf(Collections.max(valueList));
    }

    public Integer savage(double[][] profits) {
        double[] maxByColumn = new double[4];
        for (int i = 0; i < 4; i++) {
            double[] column = new double[4];
            for (int j = 0; j < 4; j++) {
                column[j] = profits[j][i];
            }
            maxByColumn[i] = Arrays.stream(column).max().getAsDouble();
        }

        double[][] riskMatrix = new double[4][4];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                riskMatrix[i][j] = maxByColumn[j] - profits[i][j];
            }
        }
        Double[] maxValues = new Double[4];
        for (int i =0; i < 4; i++){
            maxValues[i] = Arrays.stream(riskMatrix[i]).max().getAsDouble();
        }
        List<Double> valueList = new ArrayList<Double>(Arrays.asList(maxValues));
        return (valueList).indexOf(Collections.min(valueList));
    }

    public void initData() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

        try {
            List<String> pairEUR_RUB = Files.readAllLines(Paths.get("HUFRUB_210801_211128.txt"));
            List<String> pairJPY_RUB = Files.readAllLines(Paths.get("CHFRUB_210801_211128.txt"));
            List<String> pairUSD_RUB = Files.readAllLines(Paths.get("USDRUB_210801_211128.txt"));
            List<String> pairUZS_RUB = Files.readAllLines(Paths.get("NOKRUB_210801_211128.txt"));

            // Первая строка - заголовок
            sz = min(pairEUR_RUB.size(), pairJPY_RUB.size(), pairUSD_RUB.size(), pairUZS_RUB.size()) - 1;

            eurrub = new CurrencyData[sz];
            jpyrub = new CurrencyData[sz];
            usdrub = new CurrencyData[sz];
            uzsrub = new CurrencyData[sz];

            for (int i = 1; i < sz; i++) {
                String[] p_eurrub = pairEUR_RUB.get(i).replace(",", ".").split(";");
                String[] p_jpyrub = pairJPY_RUB.get(i).replace(",", ".").split(";");
                String[] p_usdrub = pairUSD_RUB.get(i).replace(",", ".").split(";");
                String[] p_uzsrub = pairUZS_RUB.get(i).replace(",", ".").split(";");

                eurrub[i-1] = new CurrencyData(sdf.parse(p_eurrub[2]), Double.parseDouble(p_eurrub[4]), Double.parseDouble(p_eurrub[7]));
                jpyrub[i-1] = new CurrencyData(sdf.parse(p_jpyrub[2]), Double.parseDouble(p_jpyrub[4]), Double.parseDouble(p_jpyrub[7]));
                usdrub[i-1] = new CurrencyData(sdf.parse(p_usdrub[2]), Double.parseDouble(p_usdrub[4]), Double.parseDouble(p_usdrub[7]));
                uzsrub[i-1] = new CurrencyData(sdf.parse(p_uzsrub[2]), Double.parseDouble(p_uzsrub[4]), Double.parseDouble(p_uzsrub[7]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    static int min(int a1, int a2, int a3, int a4) {
        int x, y;
        return a1 < (y = a2 < (x = a3 < a4 ? a3 : a4) ? a2 : x) ? a1 : y;
    }
}

class Sample {
    // выборочная дисперсия по стратегии
    public double[] d = new double[4];;
    // "выигрыш" для стратегии
    public double[] profit = new double[4];
    // индекс стратегии с максимальным риском (т.е. максимальной дисперсией)
    public int risk_strategy;
    // Состояния природы для стратегий.
    // Например, environment_state[0] - состояние природы для первой стратегии в этой конкретной выборке
    public int environment_state[] = new int[4];
}

class CurrencyData {

    public Date date;
    public double open;
    public double close;

    public CurrencyData(Date date, double open, double close) {
        this.date = date;
        this.open = open;
        this.close = close;
    }
}
