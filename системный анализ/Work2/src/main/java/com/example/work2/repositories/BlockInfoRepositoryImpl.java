package com.example.work2.repositories;

import com.example.work2.test.BlockInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BlockInfoRepositoryImpl implements BlockInfoRepository {

    private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/testproject";
    private static final String USER = "postgres";
    private static final String PASS = "";
    private Connection connection;

    private static String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS block_info (" +
            " num int NOT NULL PRIMARY KEY, data varchar(1000), prev_hash varchar(1000), data_sign bytea, sign bytea, public_key varchar(1000));";

    private static String SAVE = "INSERT INTO block_info (num, data, prev_hash, data_sign, sign, public_key) VALUES (?, ?, ?, ?, ?, ?)";
    private static String GET = "SELECT * FROM block_info WHERE num = ?";
    private static String GET_ALL = "SELECT * FROM block_info";
    private static String COUNT = "SELECT COUNT(*) FROM block_info";
    private static String FIND_ID_BY_HASH = "SELECT num FROM block_info WHERE prev_hash = ?";
    private static String UPDATE_SIGN = "UPDATE block_info set sign = ? WHERE num = ?";

    public BlockInfoRepositoryImpl() {
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement statement = connection.prepareStatement(CREATE_TABLE);
            statement.execute();

        } catch (SQLException | ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void saveBlockInfo(BlockInfo blockInfo) {
        try {
            PreparedStatement statement = connection.prepareStatement(SAVE);
            statement.setInt(1, blockInfo.getBlockNum());
            statement.setString(2, new Gson().toJson(blockInfo.getData()));
            statement.setString(3, blockInfo.getPrevHash());
            statement.setBytes(4, blockInfo.getDataSign());
            statement.setBytes(5, blockInfo.getSign());
            statement.setString(6, blockInfo.getPublicKey());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public BlockInfo getData(Integer id) {

        try {
            PreparedStatement statement = connection.prepareStatement(GET);
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                BlockInfo info = new BlockInfo(
                        resultSet.getInt(1)
                );
                List<String> list = new Gson().fromJson(resultSet.getString(2), new TypeToken<List<String>>() {
                }.getType());
                info.setData(list);
                info.setPrevHash(resultSet.getString(3));
                info.setDataSign(resultSet.getBytes(4));
                info.setSign(resultSet.getBytes(5));
                info.setPublicKey(resultSet.getString(6));
                return info;
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<BlockInfo> getBlockListInfo() {
        try {
            PreparedStatement statement = connection.prepareStatement(GET_ALL);
            ResultSet resultSet = statement.executeQuery();
            ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
            int columnCount = resultSetMetaData.getColumnCount();
            List<BlockInfo> blockInfoList = new ArrayList<>();

            while (resultSet.next()) {
                BlockInfo info = new BlockInfo(
                        resultSet.getInt(1)
                );
                List<String> list = new Gson().fromJson(resultSet.getString(2), new TypeToken<List<String>>() {
                }.getType());
                info.setData(list);
                info.setPrevHash(resultSet.getString(3));
                info.setDataSign(resultSet.getBytes(4));
                info.setSign(resultSet.getBytes(5));
                info.setPublicKey(resultSet.getString(6));
                blockInfoList.add(info);
            }
            return blockInfoList;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Integer count() {
        try {
            PreparedStatement statement = connection.prepareStatement(COUNT);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private Optional<Integer> findIdByHash(String hash) {
        try {
            PreparedStatement statement = connection.prepareStatement(FIND_ID_BY_HASH);
            statement.setString(1, hash);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return Optional.of(resultSet.getInt(1) - 1);
            } else {
                return Optional.empty();
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public BlockInfo findByHash(String hash) {
        Optional<Integer> optionalId = findIdByHash(hash);
        if (optionalId.isPresent()) {
            System.out.println(optionalId.get());
            return getData(optionalId.get());
        }

        int count = count();
        BlockInfo lastBlockInfo = getData(count - 1);

        if (lastBlockInfo == null) {
            throw new IllegalStateException("Block not fount by this hash");
        }

        return lastBlockInfo;
    }

    @Override
    public void updateSign(BlockInfo blockInfo) {
        try {
            PreparedStatement statement = connection.prepareStatement(UPDATE_SIGN);
            statement.setBytes(1, blockInfo.getSign());
            statement.setInt(2, blockInfo.getBlockNum());
            statement.execute();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
