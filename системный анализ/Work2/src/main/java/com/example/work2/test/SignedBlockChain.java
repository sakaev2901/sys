package com.example.work2.test;

import com.example.work2.repositories.BlockInfoRepository;
import com.example.work2.repositories.BlockInfoRepositoryImpl;
import org.bouncycastle.util.encoders.Hex;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.example.work2.test.Utils.generateRSAPSSSignature;

public class SignedBlockChain {

    private static final int BC_LENGTH = 10;
    private static List<BlockInfo> blockchain = new ArrayList<>();
    private static KeyPair keyPair;

    static {
        try {
            keyPair = Utils.loadKeys();
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    ;
    private static final BlockInfoRepository repository = new BlockInfoRepositoryImpl();


    public static void main(String[] args) {

        try {

            //makeBlockChain();

            //  print();

            //  System.out.println("verification result: " + verification());

            //damage();
            blockchain = getBlockChain();
            System.out.println("verification result: " + verification(blockchain));
             //save(blockchain);
            //System.out.println(getBlockChain());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean verificationBlock(BlockInfo blockInfo) throws IOException, GeneralSecurityException {
        byte[] byteData = Utils.stringListToByteArray(blockInfo.getData());
        return Utils.verifyRSAPSSSignature(keyPair.getPublic(), byteData, blockInfo.getDataSign());
    }

    public static List<BlockInfo> getBlockChain() {
        return repository.getBlockListInfo();
    }

    public static BlockInfo getBlockInfoByNum(Integer num) {
        return repository.getData(num);
    }

    public static BlockInfo findBlockInfoByHash(String hash) {
        return repository.findByHash(hash);
    }

    public static void setSign(BlockInfo blockInfo) {
        repository.updateSign(blockInfo);
    }

    public static void save(List<BlockInfo> blockInfoList) {
        for (BlockInfo blockInfo : blockInfoList) {
            repository.saveBlockInfo(blockInfo);
        }
    }

    public static BlockInfo save(BlockInfo blockInfo) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchProviderException {
        int count = repository.count();
        BlockInfo lastBlockInfo = repository.getData(count - 1);
        byte[] prevHash = Utils.getHash(lastBlockInfo);
        blockInfo.setPrevHash(Utils.byteArrayToHexString(prevHash));
        blockInfo.setBlockNum(count);
        repository.saveBlockInfo(blockInfo);
        return blockInfo;
    }

    public static void makeBlockChain() {
        byte[] prevHash = null;

        for (int i = 0; i < BC_LENGTH; i++) {
            BlockInfo blockInfo = new BlockInfo(i);
            blockInfo.getData().add("{\"data\":\"data " + i + "\"}");
            blockInfo.getData().add("{\"timestamp\":\"" + new Date() + "\"}");
            blockInfo.setPrevHash(Utils.byteArrayToHexString(prevHash));
            try {
                byte[] byteData = Utils.stringListToByteArray(blockInfo.getData());
                blockInfo.setDataSign(Utils.generateRSAPSSSignature(keyPair.getPrivate(), byteData));

                byte[] hash = Utils.getHash(blockInfo);

                blockInfo.setSign(Utils.generateRSAPSSSignature(keyPair.getPrivate(), hash));
                blockInfo.setPublicKey(Utils.byteArrayToHexString(keyPair.getPublic().getEncoded()));
                prevHash = hash;
            } catch (Exception e) {
                e.printStackTrace();
            }


            blockchain.add(blockInfo);
        }
    }

    public static void print() throws NoSuchAlgorithmException, NoSuchProviderException, UnsupportedEncodingException {
        for (int i = 0; i < BC_LENGTH; i++) {
            BlockInfo bi = blockchain.get(i);
            System.out.println("===================== " + bi.getBlockNum() + " =============================");
            System.out.println("prev hash: " + (bi.getPrevHash() != null ? bi.getPrevHash() : ""));
            for (String s : bi.getData()) System.out.println(s);
            System.out.println("digest: " + new String(Hex.encode(Utils.getHash(bi))));
            System.out.println("signature: " + new String(Hex.encode(bi.getSign())));
            System.out.println("data_signature: " + new String(Hex.encode(bi.getDataSign())));
            System.out.println();
        }
    }

    public static boolean verification() throws Exception {
        return verification(getBlockChain());
    }

    public static boolean verification(List<BlockInfo> blockchain) throws Exception {

        byte[] prevHash = Utils.getHash(blockchain.get(0));
        for (int i = 1; i < repository.count(); i++) {
            if (!Utils.byteArrayToHexString(prevHash).equals(blockchain.get(i).getPrevHash())) {
                return false;
            }

            byte[] hash = Utils.getHash(blockchain.get(i));

            BlockInfo blockInfo = blockchain.get(i);
            if (!Utils.verifyRSAPSSSignature(Utils.byteToPublicKey(blockInfo.getPublicKey().getBytes()), hash, blockchain.get(i).getSign())) {
                return false;
            }

            byte[] byteData = Utils.stringListToByteArray(blockchain.get(i).getData());

            if (!Utils.verifyRSAPSSSignature(keyPair.getPublic(), byteData, blockchain.get(i).getDataSign())) {
                return false;
            }

            prevHash = hash;
        }

        return true;
    }

    public static void damage() {
        blockchain.get(3).getData().remove(0);
        blockchain.get(3).getData().add(0, "{\"data\":\"damaged data\"}");
    }

    public static KeyPair getKeyPair() {
        return keyPair;
    }
}

