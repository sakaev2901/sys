package com.example.work2.controllers;

import com.example.work2.responses.TimeStampResp;
import com.example.work2.responses.TimeStampToken;
import com.example.work2.test.BlockInfo;
import com.example.work2.test.SignedBlockChain;
import com.example.work2.test.Utils;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.web.bind.annotation.*;

import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.example.work2.test.Utils.generateRSAPSSSignature;

@RestController
public class DigitHashController {

    @ResponseBody
    @RequestMapping(value = "/ts")
    public TimeStampResp getTS(@RequestParam(name = "digest", required = false) String digestStr) {
        TimeStampResp resp = null;
        try {
            PrivateKey privateKey = SignedBlockChain.getKeyPair().getPrivate();
            String date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SX").format(new Date());

            byte[] sign = generateRSAPSSSignature(Utils.loadKeys().getPrivate(), Hex.decode(digestStr));
            BlockInfo blockInfo = SignedBlockChain.findBlockInfoByHash(digestStr);

            blockInfo.setSign(sign);
            SignedBlockChain.setSign(blockInfo);
            TimeStampToken ts = new TimeStampToken(date, new String(Hex.encode(sign)));
            resp = new TimeStampResp(0, "", ts);

            System.out.println("------");

            System.out.println(Utils.verifyRSAPSSSignature(Utils.loadKeys().getPublic(), Utils.getHash(blockInfo), generateRSAPSSSignature(Utils.loadKeys().getPrivate(), Hex.decode(digestStr))));
            System.out.println("------");
        } catch (GeneralSecurityException e) {
            resp = new TimeStampResp(2, "rejection", null);
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resp;
    }
}
