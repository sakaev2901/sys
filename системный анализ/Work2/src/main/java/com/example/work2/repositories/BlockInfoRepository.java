package com.example.work2.repositories;


import com.example.work2.test.BlockInfo;

import java.util.List;

public interface BlockInfoRepository {

    void saveBlockInfo(BlockInfo blockInfo);
    BlockInfo getData(Integer id);
    List<BlockInfo> getBlockListInfo();
    public Integer count();
    BlockInfo findByHash(String hash);
    void updateSign(BlockInfo blockInfo);
}
