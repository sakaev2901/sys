package com.example.work2.requests;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BlockInfoRequest {
    private List<String> data = new ArrayList<>();
    private byte[] signature;
    private String publicKey;
}
