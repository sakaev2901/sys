package com.example.work2.controllers;

import com.example.work2.requests.BlockInfoRequest;
import com.example.work2.responses.BlockInfoResponse;
import com.example.work2.test.BlockInfo;
import com.example.work2.test.SignedBlockChain;
import com.example.work2.test.Utils;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.List;

import static com.example.work2.test.Utils.generateRSAPSSSignature;

@RestController
public class SaveController {

    @PostMapping("/save")
    public ResponseEntity<BlockInfoResponse> save(@RequestBody BlockInfoRequest request) {

        try {
            BlockInfo blockInfo = new BlockInfo();
            blockInfo.setData(request.getData());
            blockInfo.setPublicKey(request.getPublicKey());
            blockInfo.setDataSign(request.getSignature());
            blockInfo = SignedBlockChain.save(blockInfo);
            BlockInfoResponse blockInfoResponse = BlockInfoResponse.builder()
                    .blockNum(blockInfo.getBlockNum())
                    .data(blockInfo.getData().toString())
                    .dataSign(Utils.byteArrayToHexString(blockInfo.getDataSign()))
                    .sign(Utils.byteArrayToHexString(blockInfo.getSign()))
                    .hash(Utils.byteArrayToHexString(Utils.getHash(blockInfo)))
                    .build();

            System.out.println("------");
            List<BlockInfo> list = SignedBlockChain.getBlockChain();
            BlockInfo bf = list.get(list.size() - 1);

            System.out.println(Utils.verifyRSAPSSSignature(Utils.loadKeys().getPublic(), Utils.getHash(blockInfo), generateRSAPSSSignature(Utils.loadKeys().getPrivate(), Hex.decode(blockInfoResponse.getHash()))));
            System.out.println("------");
            return ResponseEntity.ok().body(blockInfoResponse);
        } catch (NoSuchAlgorithmException | NoSuchProviderException | UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        } catch (GeneralSecurityException e) {
            throw new IllegalStateException(e);
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }
}
