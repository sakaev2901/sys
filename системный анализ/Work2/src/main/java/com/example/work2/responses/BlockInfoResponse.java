package com.example.work2.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BlockInfoResponse {
    private int blockNum;
    private String data;
    private String hash;
    private String dataSign;
    private String sign;
}
