package com.example.work2.controllers;

import com.example.work2.responses.TimeStampResp;
import com.example.work2.test.BlockInfo;
import com.example.work2.test.SignedBlockChain;
import com.example.work2.test.Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;


@RestController
public class VerifyController {

    @ResponseBody
    @RequestMapping(value = "/verify")
    public String getPublic() {
        try {
            return String.valueOf(SignedBlockChain.verification());
        } catch (Exception e) {
            throw new IllegalStateException(e);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/verifyBlock")
    public String getTS(@RequestParam(name = "num", required = false) Integer num) {
        try {
            BlockInfo blockInfo = SignedBlockChain.getBlockInfoByNum(num);
            Boolean bool = SignedBlockChain.verificationBlock(blockInfo);
            return String.valueOf(bool);
        } catch (IOException | GeneralSecurityException e) {
            throw new IllegalStateException(e);
        }
    }
}
