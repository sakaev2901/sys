package com.example.work2.controllers;

import com.example.work2.test.SignedBlockChain;
import com.example.work2.test.Utils;
import org.bouncycastle.util.encoders.Hex;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PublicKeyController {

    @ResponseBody
    @RequestMapping(value = "/public")
    public String getPublic() {
        return Utils.byteArrayToHexString(SignedBlockChain.getKeyPair().getPublic().getEncoded());
    }
}
