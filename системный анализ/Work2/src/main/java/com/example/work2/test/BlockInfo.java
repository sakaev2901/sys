package com.example.work2.test;

import java.util.ArrayList;
import java.util.List;

public class BlockInfo {
    private int blockNum;
    private List<String> data = new ArrayList<>();
    private String prevHash;
    private byte[] dataSign;
    private byte[] sign;
    private String publicKey;

    public BlockInfo(){}

    public BlockInfo(int blockNum) {
        this.blockNum = blockNum;
    }

    public int getBlockNum() {
        return blockNum;
    }

    public void setBlockNum(int blockNum) {
        this.blockNum = blockNum;
    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public String getPrevHash() {
        return prevHash;
    }

    public void setPrevHash(String prevHash) {
        this.prevHash = prevHash;
    }

    public byte[] getSign() {
        return sign;
    }

    public void setSign(byte[] sign) {
        this.sign = sign;
    }

    public byte[] getDataSign() {
        return dataSign;
    }

    public void setDataSign(byte[] dataSign) {
        this.dataSign = dataSign;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String toString(){
        return blockNum + " " + data;
    }
}
