import com.example.work2.requests.BlockInfoRequest;
import com.example.work2.requests.SendRequest;
import com.example.work2.test.Utils;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyPair;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


//37D1B75D92AE0412C2E8E3CB9552DC19A7A6BDFB19EE299C2B208801C291D74AA68C56DEE779B39BA39349A47415A4595FDF9FE38B2904179A045FFA82D2E785C12BC61EEB76ADC64B72792CF3E6BCD0C611CBC15E10E5984C80AFBBFEDAB8E7BD0D3022D060B3B6DBFF47B82A5A4DEC85A51507867BA36573893A18E504D5EA
public class TestClient {

    static final String digest = "EB1584C2AFE29FED37280FC14AFC6E50E38BC58383DEDF4679BDC1DF469715DC";
    static final String data = "{\"w11\":\"0.5213213\",\"w12\":\"0.123124214\",\"w21\":\"0.1234435,\",\"w22\":\"1.123123231\",\"v11\":\"1.84993994\",\"v12\":\"0.0000008493\",\"v13\":\" 1.23238553\",\"v21\":\"0.745353546\",\"v22\":\"0.00858366\",\"v23\":\"0.54673466\",\"w1\":\"0.000235355\",\"w2\":\"0.0073953\",\"w3\":\"0.1838994\",\"e\":\"0.29144745677294837\",\"publickey\":\"30819f300d06092a864886f70d010101050003818d0030818902818100a811365d2f3642952751029edf87c8fa2aeb6e0feafcf800190a7dd2cf750c63262f6abd8ef52b251c0e10291d5e2f7e6682de1aae1d64d4f9b242050f898744ca300a44c4d8fc8af0e7a1c7fd9b606d7bde304b29bec01fbef554df6ba1b7b1ec355e1ff68bd37f3d40fb27d1aa233fe3dd6b63f7241e734739851ce8c590f70203010001\"}\n";
    // {"prevhash":" ... ","data":{"w11":"1","w12":"1", ... ,"publickey":"..."}, ... }.
    //NeuralNetwork nn = new NeuralNetwork(0.5213213, 0.123124214, 0.1234435, 1.123123231, 1.84993994, 0.0000008493, 1.23238553, 0.745353546,0.00858366, 0.54673466, 0.000235355, 0.0073953, 0.1838994);
    public static void main(String[] args) throws Exception {

        save();
        //readAndVerifyTimeStamp(digest);
        //verification();
        //verificationBlock(13);
        //getPublicKey();
//        send();

    }

    public static void getPublicKey() throws Exception{
        URL url = new URL("http://localhost:8080/public");

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");
        print(con);
    }

    public static void verificationBlock(Integer num) throws Exception {
        URL url = new URL("http://localhost:8080/verifyBlock?num=" + num);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");
        print(con);
    }

    public static void verification() throws Exception{
        URL url = new URL("http://localhost:8080/verify");

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");
        print(con);
    }

    public static void readAndVerifyTimeStamp(String digest) throws Exception {

        KeyPair keyPair = Utils.loadKeys();
        PublicKey publicKey = keyPair.getPublic();

        URL url = new URL("http://localhost:8080/ts?digest=" + digest);

        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("GET");

        print(con);
    }

    public static void send() throws Exception {
        KeyPair keyPair = Utils.loadKeys();
        SendRequest request = new SendRequest();
        request.setData(data);
        byte[] byteData = Utils.stringListToByteArray(request.getData());
        request.setSignature(Utils.byteArrayToHexString(Utils.generateRSAPSSSignature(keyPair.getPrivate(), byteData)));
        System.out.println(Utils.byteArrayToHexString(Utils.generateRSAPSSSignature(keyPair.getPrivate(), byteData)));
        request.setInfo("Салимов Наиль 11-802");


        String json = new Gson().toJson(request);
        System.out.println(json);

    }

    public static void save() throws Exception {

        KeyPair keyPair = Utils.loadKeys();
        BlockInfoRequest request = new BlockInfoRequest();
        List<String> list = new ArrayList<>();
            list.add("{\"data\":\"data " + "TTTT" + "\"}");
        list.add("{\"timestamp\":\"" + new Date() + "\"}");
        request.setData(list);
        request.setPublicKey(Utils.byteArrayToHexString(keyPair.getPublic().getEncoded()));
        byte[] byteData = Utils.stringListToByteArray(request.getData());
        request.setSignature(Utils.generateRSAPSSSignature(keyPair.getPrivate(), byteData));
        URL url = new URL("http://localhost:8080/save");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        String json = new Gson().toJson(request);
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = json.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        print(con);
        String d= "%7B%0A%22prevhash%22%3A%22dbeb24bf54fea1da07e5068cd46abe1582ef477a264adf098ab09aca5cb2132f%22%2C%0A%22data%22%3A%20%7B%0A%22w11%22%3A%20%22-0.3785078981223825%22%2C%0A%22w12%22%3A%20%22-0.40679400724158354%22%2C%0A%22w21%22%3A%20%220.4382467363903131%22%2C%0A%22w22%22%3A%20%220.3385126937015749%22%2C%0A%0A%22v11%22%3A%20%22-0.9534761123612924%22%2C%0A%22v12%22%3A%20%220.21041810033405906%22%2C%0A%22v13%22%3A%20%22-0.40264471078098685%22%2C%0A%22v21%22%3A%20%220.5682906225142552%22%2C%0A%22v22%22%3A%20%22-0.3369562769827137%22%2C%0A%22v23%22%3A%20%220.7262122803111863%22%2C%0A%22w1%22%3A%20%220.28463309744853027%22%2C%0A%22w2%22%3A%20%220.3070970924357823%22%2C%0A%22w3%22%3A%20%220.4270165790293512%22%2C%0A%22e%22%3A%20%220.015105228791907083%22%2C%0A%22publickey%22%3A%20%2230819f300d06092a864886f70d010101050003818d0030818902818100a811365d2f3642952751029edf87c8fa2aeb6e0feafcf800190a7dd2cf750c63262f6abd8ef52b251c0e10291d5e2f7e6682de1aae1d64d4f9b242050f898744ca300a44c4d8fc8af0e7a1c7fd9b606d7bde304b29bec01fbef554df6ba1b7b1ec355e1ff68bd37f3d40fb27d1aa233fe3dd6b63f7241e734739851ce8c590f70203010001%22%0A%7D%2C%0A%22signature%22%3A%220239C246714005D6CCEEBEBCB8CCEC58A836928BC09361F0BAF92CC71554AECB424E8E9CA43E49EDB8381BEFD90E049EEA876DB3A44DA5726867D0731CC6EF72600B3556EE6849CBC1E9A04DEFCA75A39811D8E9A4100639B3C4DB23478AAC6BBF8C151C85BF02085EE9E1A7230FBB4BB742AB828BE55211A4A1B2F0F51C3EA8%22%2C%0A%22info%22%3A%22%D0%A1%D0%B0%D0%BB%D0%B8%D0%BC%D0%BE%D0%B2%20%D0%9D%D0%B0%D0%B8%D0%BB%D1%8C%2011-802%22%0A%7D";
        URL url1 = new URL("http://188.93.211.195/newblock?block="+d);
        con = (HttpURLConnection) url1.openConnection();
        con.setRequestMethod("GET");
    }

    public static void print(HttpURLConnection con ) throws Exception{
        try (BufferedReader br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }
    }
}
