import com.example.work2.Game;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Main {
    private static final int COUNT_EPOCH = 100;
    private static final double SPEED = 0.001;
    private static final String WEIGHTFILE = "v2.csv";
    private static final String XDATAFILE = "xdata.csv";
    private static final String YDATAFILE = "ydata04.csv";

    private double[][] x; //массив с набором обучающих входных данных
    private double[] y;     //массив с правильными значениями для набора обучающих входных данных
    private double w[];

    /*
    E: 8.86560252733819E-5
----------
0.8883436475532931
-0.8888081397828403
0.30008483923049256
-0.9756214553908479
-0.5528559230220609
0.06496348902321658
0.6939826997615139
0.6518521823353446
-0.7614181124190857
-0.08606061937563447
-0.6572983701699676
0.5893202377495421
0.9690744503711125

{"w11":"-0.3785078981223825","w12":"-0.40679400724158354","w21":"0.4382467363903131","w22":"0.3385126937015749","v11":"-0.9534761123612924","v12":"0.21041810033405906","v13":"-0.40264471078098685","v21":"0.5682906225142552","v22":"-0.3369562769827137","v23":"0.7262122803111863","w1":"0.28463309744853027","w2":"0.3070970924357823","w3":"0.4270165790293512","e":"0.015105228791907083","publickey":"30819f300d06092a864886f70d010101050003818d0030818902818100a811365d2f3642952751029edf87c8fa2aeb6e0feafcf800190a7dd2cf750c63262f6abd8ef52b251c0e10291d5e2f7e6682de1aae1d64d4f9b242050f898744ca300a44c4d8fc8af0e7a1c7fd9b606d7bde304b29bec01fbef554df6ba1b7b1ec355e1ff68bd37f3d40fb27d1aa233fe3dd6b63f7241e734739851ce8c590f70203010001"}


E: 0.015105228791907083
-0.3785078981223825
-0.40679400724158354
0.4382467363903131
0.3385126937015749

-0.9534761123612924
0.21041810033405906
-0.40264471078098685
0.5682906225142552
-0.3369562769827137
0.7262122803111863

0.28463309744853027
0.3070970924357823
0.4270165790293512

     */

    public Main() {


        loadData();
        start();

//        while (error() > 1.0) {
//           start();
//        }
        //w = new double[] {-1.2908596, -0.61649865, -0.76196724, -0.11153736, -0.657419 ,  -0.39429832,  -0.7321624,  1.1040673 ,  0.5443073  , 0.42947778, 0.6644227 , -0.72721857, 0.72742397};
        System.out.println("---");
        System.out.println("E: " + error());


        for (int i =0; i < 13; i++) {
            System.out.println(w[i]);
        }

    }

    public double[] start(){
        double best[] = new double[13];
        double minE = 10000000000.0;
        for (int j=0; j <1000000.0; j++) {
            w = new double[13];
            Random random = new Random();
            for (int i = 0; i < 13; i++) {
                boolean a = random.nextBoolean();
                w[i] = Math.random();
                if (a) {
                    w[i] *= -1;
                }
            }
            double e = error();
            //System.out.println(e);
            if (minE > e) {
                minE = e;
                best = w;
            }
        }
        //System.out.println(minE);
        return best;

    }

    public double getValue(double x1, double x2) {
        double z1 = w[0] * x1 + w[2] * x2;
        double z2 = w[1] * x1 + w[3] * x2;

        double n1 = w[4] * f(z1) + w[7] * f(z2);
        double n2 = w[5] * f(z1) + w[8] * f(z2);
        double n3 = w[6] * f(z1) + w[9] * f(z2);

        double m = w[10] * f(n1) + w[11] * f(n2) + w[12] * f(n3);
        return f(m);
    }

    public double error() {

        double e = 0.0;

        for (int i = 0; i < y.length; i++) {
            double a = (getValue(x[i][0], x[i][1])- y[i]);
            double r = a * a;
            e += r;
        }
        return e*100;
    }

    public void changeW(int index) {
        double optimalError = 10000.0;
        double j = 0.0;
        for (double i = -1.0; i <= 1.0; i += SPEED) {
            w[index] = i;
            j = i;
            double e = error();

            if (e < optimalError) {
                optimalError = e;

            } else if (e > optimalError) {
                break;
            }

        }
        System.out.println("W[" + index + "] = " + j + " : " + optimalError);
    }


    public static void main(String[] args) {

        Main main = new Main();
    }


    public void loadData() {
        try {
            List<String> x_lines =
                    Files.readAllLines(Paths.get(XDATAFILE));

            x = new double[x_lines.size()][2];

            for (int i = 0; i < x_lines.size(); i++) {
                String[] x_pair = x_lines.get(i).split(";");
                x[i][0] = Double.parseDouble(x_pair[0]);
                x[i][1] = Double.parseDouble(x_pair[1]);
            }

            List<String> y_lines =
                    Files.readAllLines(Paths.get(YDATAFILE));

            y = new double[y_lines.size()];

            for (int i = 0; i < y_lines.size(); i++) {
                y[i] = Double.parseDouble(y_lines.get(i));
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Неверный формат входных данных!");
            System.exit(-1);
        }
    }

    public double f(double x) {
        return (1 / (1 + Math.exp(-x)));
    }

}
